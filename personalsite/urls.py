from django.conf.urls import url
from django.contrib import admin
from mysite import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^writings/', views.writings, name='writings'),
    url(r'^projects/', views. projects, name='projects'),
    url(r'^me/', views.me, name='me'),
    url(r'^connect/', views.connect, name='connect'),
    # url(r'^write/', views.write_article, name='write_article'),
    # url(r'^article/(?P<pk>\d+)/', views.article, name='article'),
    url(r'^amanvats/admin/', admin.site.urls),
]
