from django.db import models
from django.utils.text import Truncator
from markdown import markdown
from django.utils.html import mark_safe
# Create your models here.


class Question(models.Model):
    question = models.TextField(max_length=210, blank=False, help_text="Please type the query you have for me...")
    mail_id = models.EmailField(blank=False)


class Articles(models.Model):
    title = models.TextField(max_length=500)
    content = models.TextField(max_length=15000)
    # time = models.TimeField(default="0")

    def get_short(self):
        truncated_message = Truncator(self.content)
        return mark_safe(markdown(truncated_message.chars(603)))

    def __str__(self):
        return self.title

    def get_message_as_markdown(self):
        return mark_safe(markdown(self.content, safe_mode='escape'))
