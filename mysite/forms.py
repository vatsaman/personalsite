from django.forms import ModelForm
from .views import Articles


class ArticleForm(ModelForm):
    class Meta:
        model = Articles
        fields = ['content', 'title']
