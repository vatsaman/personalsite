from django.shortcuts import render, redirect
from .models import Articles
from .forms import ArticleForm
from django.contrib.auth.decorators import login_required
# Create your views here.


def home(request):
    return render(request, 'home.html', {})


def writings(request):
    articles = Articles.objects.all()
    return render(request, 'writings.html', {'articles':articles})


def projects(request):
    return render(request, 'projects.html', {})


def me(request):
    return render(request, 'me.html', {})


@login_required()
def write_article(request):
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            topic = form.save()
            return redirect('home')
    else:
        form = ArticleForm()
    return render(request, 'new_artice.html', {'form': form})


def article(request, pk):
    article_content = Articles.objects.get(pk=pk)
    return render(request, 'fst_article.html', {'article': article_content})


def connect(request):
    return render(request, 'connect.html', {})









